const button = document.getElementById("enter");
const input = document.getElementById("userInput");
const ul = document.querySelector("ul");
const listItems = document.querySelectorAll("li");
//SELECTATI DROPDOWN-UL DIN HTML SI BUTOANELE DE FILTRARE
const dropdown = document.querySelectorAll("dropdownSelect");
const buttons = document.querySelectorAll("btnContainer");

function inputLength() {
  return input.value.length;
}

function createListElement() {
  const li = document.createElement("li");
  const span = document.createElement("span");
  const icon = document.createElement("i");

  icon.classList.add("fas", "fa-trash");
  span.className = "shopping-item-name";
  span.textContent = input.value;
  li.classList = "filterLi";

  li.appendChild(span);
  li.appendChild(icon);

  //Adaugati noului li creat clasa corespunzatoare categoriei din care face parte

  ul.appendChild(li);
  var produs = document.getElementById("dropdownSelect").value;
  if (produs == "lactate") {
    li.classList.add("lactate");
  } else if (produs == "carne") {
    li.classList.add("carne");
  } else if (produs == "sucuri") {
    li.classList.add("sucuri");
  }
  input.value = "";
}

function deleteListElement(target) {
  ul.removeChild(target.parentElement);
}
function addListItemAfterClick() {
  if (inputLength() > 0) {
    createListElement();
  }
}
function addListItemAfterKeypress(event) {
  if (inputLength() > 0 && event.key === "Enter") {
    createListElement();
  }
}

button.addEventListener("click", addListItemAfterClick);
input.addEventListener("keypress", addListItemAfterKeypress);

document.addEventListener("click", (event) => {
  if (event.target.classList.contains("fa-trash")) {
    deleteListElement(event.target);
  } else if (event.target.classList.contains("shopping-item-name")) {
    event.target.classList.toggle("done");
  } else {
    return;
  }
});

//FUNCTIE NUMITA filterItems in care sa filtrati elementele din pagina
filterItems("all");
function filterItems(c) {
  var x, i;
  x = document.getElementsByClassName("filterLi");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    AddClass(x[i], "not-show");
    if (x[i].className.indexOf(c) > -1) RemoveClass(x[i], "not-show");
  }
}

function AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

function RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}
